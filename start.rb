require 'io/console'
require './app'
require './pull_request/reviwer.rb'

puts "Enter GitHub Login:"
login = gets.chomp
puts "Enter GitHub Password:"
password = STDIN.noecho(&:gets).chomp
puts "Enter GitHub Repo or press Enter for default 'rails/rails':"
repo = gets.chomp

App::Config.auth[:login]    = login
App::Config.auth[:password] = password
App::Config.repo            = repo unless repo.empty?

PullRequest::Reviwer.call()

