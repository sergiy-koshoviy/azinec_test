# Pull Request Reviewer

This application check all opened pull requests from your repo and looking for
which of them is not optimized

## Start App

    $ ruby start.rb
    Enter GitHub Login:
    Enter GitHub Password:
    Enter GitHub Repo or press Enter for default 'rails/rails':
    
GitHub Auth needed for work with a lot of requests.

## Result

You can find result of work in **pull_requests.json** file