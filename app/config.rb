class App
  class Config
    @@auth = { login: '', password: '' }
    @@repo = 'rails/rails'

    REPORT_PATH = '../report.json'.freeze

    def self.auth
      @@auth
    end
    
    def self.repo
      @@repo
    end

    def self.report_path
      REPORT_PATH
    end

    def self.repo=(repo)
      @@repo=repo
    end
  end
end
