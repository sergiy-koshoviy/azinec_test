module PullRequest
  class Reviewer
    class Filter < App
      def self.call(collection)
        new(collection).call
      end

      attr_reader :collection

      def initialize(collection)
        super

        @collection = collection
      end

      def call
        p 'Start pull request filtering...'

        filtered = []
        collection.each do |pull_request|
          p "Processed Pull Request #{pull_request['id']}"

          commits = http_get(pull_request['commits_url'])

          next if commits.count < 1

          not_optimized_files = commits_not_optimized(commits)

          next if not_optimized_files.empty?

          filtered << {
            pull_request_url: "#{pull_request['html_url']}/files",
            files: not_optimized_files
          }
        end

        self.result = filtered

        self
      end

      private

      def commits_not_optimized(commits)
        commits_sha = [commits.first['parents'].first['sha']] | commits.map{|commit| commit['sha']}
        updated_codes = {}
        result = []

        (0..commits_sha.size-2).each do |e|
          url  ="https://api.github.com/repos/#{App::Config.repo}/compare/#{commits_sha[e]}...#{commits_sha[e+1]}"
          diff = http_get(url)
          updated_files = {}

          next if diff['files'].nil?

          diff['files'].each do |file|
            lines = file['patch']&.split("\n")
            next if lines.nil?

            updated_files[file['filename']] = [] if updated_files[file['filename']].nil?
            prev_end_index = 0

            lines.each_with_index do |e, i|
              if (e.include?('@@') && !i.zero?) || i == lines.size-1
                number_updated_string = /\d+/.match(lines[prev_end_index]).to_s.to_i
                count_strings         = lines[prev_end_index..i-1].size

                updated_files[file['filename']] << (number_updated_string..number_updated_string + count_strings)

                prev_end_index = i
              end
            end
          end

          updated_files.each do |file, lines|
            updated_codes[file] = [] if updated_codes[file].nil?
            updated_codes[file] = updated_codes[file] << lines.map(&:to_a).flatten.uniq
          end
        end

        # {"activerecord/test/cases/base_test.rb"=>[[1509, 1510, 1511, 1512, 1513], [1514, 1515, 1516, 1517]]}
        # where subarrays - updated lines in some commit
        updated_codes.each do |file, commits|
          prev_collection = []
          commits.each do |commit|
            searched_lines = (prev_collection & commit)
            if searched_lines.any?
              result << { file: file, lines: searched_lines }
              break
            else
              prev_collection = prev_collection + commit
            end
          end
        end

        result
      end
    end
  end
end
