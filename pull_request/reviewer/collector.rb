module PullRequest
  class Reviewer
    class Collector < App
      GITHUB_API_URL = "https://api.github.com/repos/#{::App::Config.repo}/pulls?state=open"
      PAGE           = 'page='.freeze
      PER_PAGE       = 'per_page=100'.freeze

      def self.call
        new.call
      end

      attr_accessor :result, :data_processed, :page, :ok, :errors

      def initialize
        super

        @data_processed = true
        @page           = 1
        @result         = []
      end

      def call
        p 'Reciving data of opened pull requests...'

        while data_processed do
          p "Start processing #{self.page} page..."

          data = fetch_pull_requests_from_page(page)
          attach_data_to_result_or_close_loop(data)

          p "End processing #{self.page} page..."
          self.page += 1
        end

        p 'Receiving data is complete successful!'

        self
      end

      private

      def request_url(page=1)
        "#{GITHUB_API_URL}&#{PER_PAGE}&#{PAGE}#{page}"
      end

      def fetch_pull_requests_from_page(page)
        http_get(request_url(page))
      end

      def attach_data_to_result_or_close_loop(data)
        self.data_processed = false and return if data.empty?

        self.result = result | data
      rescue
        self.ok = false
        self.data_processed = false
        self.errors << (data.nil? ? 'Data is invalid' : data[0..200])
      end
    end
  end
end
