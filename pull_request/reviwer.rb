require './pull_request/reviewer/collector'
require './pull_request/reviewer/filter'

module PullRequest
  class Reviwer < App
    RESULT_FILE_NAME = 'pull_requests.json'.freeze

    def self.call
      new.call
    end

    def initialize
      @data = nil
    end

    def call
      grab_all_opened_pull_request
      filter_pull_requests
      show_requests_count
      save_result_to_file
    end

    private

    attr_accessor :data

    def grab_all_opened_pull_request
      self.data = execute_step(Reviewer::Collector.call).result
    end

    def filter_pull_requests
      self.data = execute_step(Reviewer::Filter.call(data)).result
    end

    def show_requests_count
      puts "Count of Pull Requests: #{data.count}"
    end

    def save_result_to_file
      File.write(RESULT_FILE_NAME, data)
    end
  end
end
