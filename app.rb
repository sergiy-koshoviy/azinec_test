require './gemfile'
require './app/config'

class App
  attr_accessor :result, :ok, :errors

  def initialize(params = {})
    @result = nil
    @ok     = true
    @errors = []
  end

  def execute_step(step)
    return step if step.ok?

    p "ERROR: #{step.errors}"
    Kernel.exit(false)
  end

  alias_method :ok?, :ok

  def http_get(url)
    #res = Net::HTTP.get URI(url)
    #JSON.parse(res)
    uri = URI(url)
    req = Net::HTTP::Get.new(uri)
    req.basic_auth App::Config.auth[:login], App::Config.auth[:password]
    res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: true) {|http|
      http.request(req)
    }
    JSON.parse(res.body)
  end
end
